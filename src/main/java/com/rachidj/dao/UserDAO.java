package com.rachidj.dao;

import com.rachidj.controllers.EMF;
import com.rachidj.dao.interfaces.IDAOByCriteria;
import com.rachidj.entities.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UserDAO implements IDAOByCriteria<User> {

    EntityManager em = EMF.createEntityManger();

    @Override
    public User getByCredentials(String username, String password) {
        Query query =  em.createQuery(
                "FROM User u WHERE u.username=:username and u.password=:password",
                User.class
        );
        query.setParameter("username", username);
        query.setParameter("password", password);

        return (User) query.getSingleResult();
    }

    public User read(Long id) {
        return em.find(User.class, id);
    }

}
