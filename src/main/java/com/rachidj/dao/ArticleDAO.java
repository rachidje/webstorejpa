package com.rachidj.dao;

import com.rachidj.controllers.EMF;
import com.rachidj.dao.interfaces.IDao;
import com.rachidj.entities.Article;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ArticleDAO implements IDao<Article> {

    EntityManager em = EMF.createEntityManger();

    @Override
    public List<Article> getAll() {

        Query query = em.createQuery("SELECT a FROM Article a");
        return (List<Article>) query.getResultList();
    }

    @Override
    public Long getCount() {
        return (Long) em.createNamedQuery("articles.countAll").getSingleResult();
    }

    @Override
    public Article read(Long id) {
        return em.find(Article.class, id);
    }
}
