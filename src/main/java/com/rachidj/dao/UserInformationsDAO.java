package com.rachidj.dao;

import com.rachidj.controllers.EMF;
import com.rachidj.dao.interfaces.IDao;
import com.rachidj.entities.UserInformations;

import javax.persistence.EntityManager;

public class UserInformationsDAO implements IDao<UserInformations> {

    EntityManager em = EMF.createEntityManger();

    @Override
    public UserInformations read(Long id) {
        return em.find(UserInformations.class, id);
    }
}
