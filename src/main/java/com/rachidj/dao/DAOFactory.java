package com.rachidj.dao;

import com.rachidj.dao.interfaces.IDAOByCriteria;
import com.rachidj.dao.interfaces.IDao;
import com.rachidj.entities.Article;
import com.rachidj.entities.Command;
import com.rachidj.entities.User;
import com.rachidj.entities.UserInformations;

public class DAOFactory {

    public static IDao<Article> articleDAO() {
        return new ArticleDAO();
    }
    public static IDAOByCriteria<User> userDAO() { return new UserDAO();}
    public static IDao<Command> commandDAO() { return new CommandDAO();}
    public static IDao<UserInformations> userInformationsDAO() {
        return new UserInformationsDAO();
    }
}
