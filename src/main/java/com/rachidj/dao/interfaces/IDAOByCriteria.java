package com.rachidj.dao.interfaces;

public interface IDAOByCriteria<T> extends IDao<T> {

    T getByCredentials(String username, String password);
}
