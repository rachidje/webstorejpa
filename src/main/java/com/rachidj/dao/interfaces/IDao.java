package com.rachidj.dao.interfaces;

import com.rachidj.controllers.EMF;
import org.hibernate.TransactionException;

import javax.persistence.EntityManager;
import java.util.List;

public interface IDao<T> {
    default T read(Long id){return null;}
    default List<T> getAll() {return null;}
    default boolean create(T object) {
        EntityManager em = EMF.createEntityManger();
        try{
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
            return true;
        } catch (TransactionException exception){
            em.getTransaction().rollback();
            em.close();
            return false;
        } finally {
            em.close();
        }
    }
    default boolean update(T object) {return false;}
    default boolean delete(T object) {return false;}
    default Long getCount() {return 1L;}
}
