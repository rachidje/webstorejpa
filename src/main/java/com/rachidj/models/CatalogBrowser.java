package com.rachidj.models;

import com.rachidj.dao.DAOFactory;
import com.rachidj.entities.Article;
import com.rachidj.entities.CommandLine;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class CatalogBrowser {
    private Long currentPosition = 1L;

    @Getter
    private Article currentArticle;
    private Long articlesCount = DAOFactory.articleDAO().getCount();
    @Getter
    private List<CommandLine> shoppingCart = new ArrayList<>();

    public CatalogBrowser() {
        currentArticle = DAOFactory.articleDAO().read(currentPosition);
    }

    public int getShoppingCartSize() {
        int fullQuantity = 0;
        for(CommandLine line: shoppingCart) {
            fullQuantity += line.getQuantity();
        }

        return fullQuantity;
    }

    public void goPrevious() {
        if(--currentPosition < 1){
            currentPosition = articlesCount;
        }
        currentArticle = DAOFactory.articleDAO().read(currentPosition);
    }

    public void goNext() {
        if(++currentPosition > articlesCount) {
            currentPosition = 1L;
        }
        currentArticle = DAOFactory.articleDAO().read(currentPosition);
    }

    public void addToCart() {
        for(CommandLine line: shoppingCart){
            if(line.getArticle().getIdArticle() == currentArticle.getIdArticle()){
                line.increaseQuantity();
                return;
            }
        }
        shoppingCart.add(new CommandLine(currentArticle, 1));
    }
}
