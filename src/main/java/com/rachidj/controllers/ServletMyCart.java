package com.rachidj.controllers;

import com.rachidj.dao.DAOFactory;
import com.rachidj.entities.Command;
import com.rachidj.entities.CommandLine;
import com.rachidj.entities.User;
import com.rachidj.models.CatalogBrowser;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(name = "myCart", value = "/myCart")
public class ServletMyCart extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        if(session.getAttribute("connectedUser") == null){
            response.sendRedirect("login");
            return;
        }

        request.getRequestDispatcher("myCart.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        if(session.getAttribute("connectedUser") == null){
            response.sendRedirect("login");
            return;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        CatalogBrowser browser = (CatalogBrowser) session.getAttribute("catalogBrowser");
        User connectedUser = (User) session.getAttribute("connectedUser");
        List<CommandLine> lines = browser.getShoppingCart();

        Command order = new Command();

        order.setUser(connectedUser);
        order.setDate(LocalDateTime.now().format(formatter));
        order.setLines(lines);

        for(CommandLine line: lines){
            line.setCommand(order);
        }

        boolean isOrdered = DAOFactory.commandDAO().create(order);

        if(isOrdered){
            session.setAttribute("orderMessage", "your order has been placed successfully");
            request.getRequestDispatcher("myCart.jsp").forward(request, response);
        } else {
            session.setAttribute("orderMessage", "An error occurs, please try again");
            request.getRequestDispatcher("myCart.jsp").forward(request,response);
        }
    }
}
