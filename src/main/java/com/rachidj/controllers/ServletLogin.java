package com.rachidj.controllers;

import com.rachidj.dao.DAOFactory;
import com.rachidj.entities.User;
import com.rachidj.models.CatalogBrowser;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "login", value = "/login")
public class ServletLogin extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("login", "");
        request.setAttribute("password", "");
        request.getRequestDispatcher("/login.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("txtLogin");
        String password = request.getParameter("txtPassword");

        request.setAttribute("login", login);
        request.setAttribute("password", password);

        User connectedUser = DAOFactory.userDAO().getByCredentials(login, password);

        if(connectedUser != null){
            HttpSession session = request.getSession(true);
            session.setAttribute("connectedUser", connectedUser);
            session.setAttribute("catalogBrowser", new CatalogBrowser());

            request.getRequestDispatcher("/viewArticles.jsp").forward(request,response);
        } else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }
}
