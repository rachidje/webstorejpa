package com.rachidj.webstoreJPA;

import com.rachidj.dao.DAOFactory;
import com.rachidj.entities.UserInformations;

import java.time.format.DateTimeFormatter;

public class Console {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        UserInformations userInformations = DAOFactory.userInformationsDAO().read(3L);

        System.out.println(userInformations);
    }
}
