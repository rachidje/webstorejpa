package com.rachidj.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Data
@Entity
@Table(name = "t_userinformations")
public class UserInformations {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idInformations;
    private String address;
    private String city;
    @Email
    private String email;
    private String phoneNumber;

    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "idInformations")
    private User user;

    public UserInformations(String address, String city, @Email String email, String phoneNumber) {
        this.address = address;
        this.city = city;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public UserInformations() {
    }

    @Override
    public String toString() {
        return "UserInformations{" +
                "idInformations=" + idInformations +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
