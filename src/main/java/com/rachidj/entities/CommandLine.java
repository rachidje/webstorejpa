package com.rachidj.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "t_commandlines")
public class CommandLine {

    @Id @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCommandLine;

    @Getter @Setter
    @ManyToOne(targetEntity = Command.class)
    @JoinColumn(name = "idCommand", nullable = false)
    private Command command;

    @Getter @Setter
    @ManyToOne(targetEntity = Article.class)
    @JoinColumn(name = "idArticle", nullable = false)
    private Article article;

    @Getter @Setter
    private int quantity;


    public CommandLine(Article article, int quantity) {
        this.article = article;
        this.quantity = quantity;
    }

    public CommandLine(Command command, Article article, int quantity) {
        this.command = command;
        this.article = article;
        this.quantity = quantity;
    }

    public CommandLine() {
    }

    public void increaseQuantity() {
        this.quantity++;
    }

    @Override
    public String toString() {
        return "CommandLine{" +
                "idCommandLine=" + idCommandLine +
                ", idCommand=" + command +
                ", article=" + article +
                ", quantity=" + quantity +
                '}';
    }
}
