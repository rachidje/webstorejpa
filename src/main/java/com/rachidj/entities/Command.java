package com.rachidj.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "t_commands")
public class Command {

    @Id @Getter @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCommand;

    @Getter @Setter
    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "idUser", nullable = false)
    private User user;

    @Getter @Setter
    @Column(name = "commandDate")
    private String date;

    @Getter @Setter
    @OneToMany(targetEntity = CommandLine.class,
            mappedBy = "command",
            cascade = CascadeType.ALL)
    private List<CommandLine> lines = new ArrayList<>();

    public Command(User user, String date) {
        this.user = user;
        this.date = date;
    }

    public Command() {
    }

    @Override
    public String toString() {
        return "Command{" +
                "idCommand=" + idCommand +
                ", user=" + user +
                ", date='" + date + '\'' +
                ", commands=" + lines.size() +
                '}';
    }
}
