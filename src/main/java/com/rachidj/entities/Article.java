package com.rachidj.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_articles")
@NamedQuery(name = "articles.countAll", query = "SELECT COUNT(a) FROM Article a")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idArticle;
    private String description;
    private String brand;

    @Column(name = "unitaryPrice")
    private double price;

    public Article(String description, String brand, double price) {
        this.description = description;
        this.brand = brand;
        this.price = price;
    }

    public Article() {
    }

    @Override
    public String toString() {
        return "Article{" +
                "idArticle=" + idArticle +
                ", description='" + description + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                '}';
    }
}
