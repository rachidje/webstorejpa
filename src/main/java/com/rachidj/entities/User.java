package com.rachidj.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "t_users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUser;

    @Column(name = "login")
    private String username;
    private String password;
    private int connectionNumber = 0;

    // relation oneToOne avec l'entite UserInformation
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idInformations", nullable = false)
    private UserInformations userInformations;

    // relation oneToMany avec l'entite Command
    @OneToMany(mappedBy = "user")
    private List<Command> commands = new ArrayList<>();

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, int connectionNumber, UserInformations userInformations) {
        this.username = username;
        this.password = password;
        this.connectionNumber = connectionNumber;
        this.userInformations = userInformations;
    }

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idUser +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", connectionNumber=" + connectionNumber +
                ",\n userInformations=" + userInformations +
                ",\n commands=" + commands.size() +
                '}';
    }
}
