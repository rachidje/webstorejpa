<%--
  Created by IntelliJ IDEA.
  User: rachidj
  Date: 14/03/2021
  Time: 19:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Shopping Cart Summary | ${connectedUser.username}</title>
</head>
<body>
    <table border="1">
        <thead>
        <tr>
            <th>Identifier</th>
            <th>Description</th>
            <th>Brand</th>
            <th>Unitary Price</th>
            <th>Quantity</th>
            <th>Sub-Total</th>
        </tr>
        </thead>
        <tbody>
        <c:set var="totalOrder" value="${0}"/>
        <c:forEach items="${catalogBrowser.shoppingCart}" var="line">
            <tr>
                <td>${line.article.idArticle}</td>
                <td>${line.article.description}</td>
                <td>${line.article.brand}</td>
                <td>${line.article.price}</td>
                <td>${line.quantity}</td>
                <td><fmt:formatNumber
                        type="number"
                        maxFractionDigits="2"
                        value="${line.article.price * line.quantity}"/></td>
            </tr>
            <c:set var="totalOrder" value="${totalOrder + (line.article.price * line.quantity)}"/>
        </c:forEach>
        </tbody>
    </table>
    <br/>
    <br/>
    <h3>Total order :   ${totalOrder}</h3>
    <form method="post" action="myCart">
        <input type="submit" name="btnBuy" value="Buy now">
    </form>
    <p>${orderMessage}</p>
</body>
</html>
