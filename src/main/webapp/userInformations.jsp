<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: rachidj
  Date: 13/03/2021
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h3>My informations</h3><br/>
    Login:          ${connectedUser.username}<br/>
    Address:        ${connectedUser.userInformations.address}<br/>
    City:           ${connectedUser.userInformations.city}<br/>
    <br/>
    Email:          ${connectedUser.userInformations.email}<br/>
    Phone Number:   ${connectedUser.userInformations.phoneNumber}<br/>

    <br/>
    <h3>Historique des achats:</h3><br/>
    <c:forEach items="${connectedUser.commands}" var="command">
        ${command.date}
    </c:forEach>
</body>
</html>
