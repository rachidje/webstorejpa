<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All articles</title>
</head>
<body>
    <h3> View Article - <a href="userInformations">${connectedUser.username}</a></h3>
    <br/>
        Identifier: ${catalogBrowser.currentArticle.idArticle}<br/>
        Brand: ${catalogBrowser.currentArticle.brand}<br/>
        Description: ${catalogBrowser.currentArticle.description}<br/>
        Unit Price: ${catalogBrowser.currentArticle.price}<br/>
    <br/>
    <form action="viewArticles" method="post">
        <input name="btnPrevious" type="submit" value="Previous">
        &nbsp; &nbsp;
        <input name="btnAdd" type="submit" value="Add to cart">
        &nbsp; &nbsp;
        <input name="btnNext" type="submit" value="Next">
    </form>
    <br/>
    <p>${catalogBrowser.shoppingCartSize}
        article<c:if test="${catalogBrowser.shoppingCartSize gt 1}">s</c:if> in the <a href="myCart">cart</a></p>
    <form action="editArticle" method="get">
        <input name="btnEdit" type="submit" value="Edit">
    </form>
</body>
</html>
